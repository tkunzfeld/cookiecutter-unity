# Using this cookiecutter

This is a simple cookiecutter to create a C project which uses
Unity (http://www.throwtheswitch.org/unity).
It clones the Unity repository to the project folder which
is created by cookiecutter and adds a basic test which can
be run by running "make" in the "Test" directory. 
